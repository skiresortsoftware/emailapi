﻿using Domain.Entities;
using System;
using System.Linq;

namespace Infrastructure.Data.MainModule.Emails
{
    public interface IEmailRepository
    {
        Email Send(Email email);

        EmailTransaction SendTransaction(EmailTransaction emailTransaction);
    }
}
