﻿using Domain.Entities;
using Infrastructure.Data.MainModule.Models;
using Infrastructure.Data.MainModule.RysolServiceReference;
using System;
using System.Linq;
using System.Xml;

namespace Infrastructure.Data.MainModule.Emails
{
    public class EmailRepository : IEmailRepository
    {
        private readonly EmailContext _context = new EmailContext();

        private readonly RysolServiceClient _serviceClient;

        public EmailRepository()
        {
            _serviceClient = new RysolServiceClient();

        }

        public Email Send(Email email)
        {
            try
            {
                var sessionHeader = new SessionHeader();

                var sendEmailWithDynamicSubjectResult = _serviceClient.SendEmailWithDynamicSubject(sessionHeader, email.MessageId, email.Groups.ToArray(), email.Subject, CreateMessageContent());
                    
                return email;
            }
            catch
            {
                return new Email();
            }
        }


        public EmailTransaction SendTransaction(EmailTransaction emailTransaction)
        {
            var test = emailTransaction;

            return null;
        }

        private XmlDocument CreateMessageContent()
        {
            var message = "<snowreport/>";

            var messageContent = new XmlDocument();

            messageContent.LoadXml(message);

            return messageContent;
        }
    }
}
