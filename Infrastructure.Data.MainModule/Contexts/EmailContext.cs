using Domain.Entities;
using System.Data.Entity;

namespace Infrastructure.Data.MainModule.Models
{
    public partial class EmailContext : DbContext
    {
        static EmailContext()
        {
            Database.SetInitializer<EmailContext>(null);
        }

        public EmailContext()
            : base("Name=EmailContext")
        {
        }

        public DbSet<Email> Emails { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // modelBuilder.Configurations.Add(new TemplateMap());
        }
    }
}
