﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DistributedServices.Entities
{
    public class EmailDto
    {
        public string Subject { get; set; }
        public List<string> Groups { get; set; }
        public int MessageId { get; set; }
    }
}
