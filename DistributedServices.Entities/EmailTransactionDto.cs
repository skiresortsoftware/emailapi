﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DistributedServices.Entities
{
    public class EmailTransactionDto
    {
        public string EmailAddress { get; set; }
        public string Subject { get; set; }
        public List<string> Groups { get; set; }
        public int MessageId { get; set; }
        public List<Dictionary<string, string>> CustomFields { get; set; }
        public RenterDto Renter { get; set; }
        public List<RentalAccessoryDto> RentalAccessories { get; set; }
    }

    public class RenterDto
    {
        public List<PersonDto> People { get; set; }
    }

    public class PersonDto
    {
        public string Name { get; set; }

        public string Age { get; set; }

        public string Weight { get; set; }

        public string Height { get; set; }

        public string ShowSize { get; set; }

        public string HelmetSize { get; set; }

        public string Package { get; set; }

        public string OwnBoots { get; set; }

        public string DamageWaiver { get; set; }

        public string PremiumBoots { get; set; }

        public string Accessories { get; set; }

        public string Total { get; set; }
    }

    public class RentalAccessoryDto
    {
        public string Name { get; set; }

        public string Amount { get; set; }
    }
}
