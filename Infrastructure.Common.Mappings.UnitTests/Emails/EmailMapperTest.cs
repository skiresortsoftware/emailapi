﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DistributedServices.Entities;
using System.Collections.Generic;
using Infrastructure.Common.Mappings.Emails;

namespace Infrastructure.Common.Mappings.UnitTests.Emails
{
    [TestClass]
    public class EmailMapperTest
    {
        #region Fields

        private readonly Domain.Entities.Email _domainObject = new Domain.Entities.Email 
                {
                    Subject = (new Random()).Next().ToString(),
                    MessageId = 1,
                    Groups = new List<string> { "1", "2", "3", "4" }
                };

        private readonly EmailDto _dtoObject = new EmailDto
        {
            Subject = (new Random()).Next().ToString(),
            MessageId = 2,
            Groups = new List<string> { "1", "2", "3", "4" }
        };

        private readonly List<Domain.Entities.Email> _domainObjects = new List<Domain.Entities.Email>()
        {
                new Domain.Entities.Email
                {
                    Subject = (new Random()).Next().ToString(),
                    MessageId = 1,
                    Groups = new List<string> { "1", "2", "3", "4" }
                },
                new Domain.Entities.Email
                {
                    Subject = (new Random()).Next().ToString(),
                    MessageId = 2,
                    Groups = new List<string> { "1", "2", "3", "4" }
                }
            };


        private readonly List<EmailDto> _dtoObjects = new List<EmailDto>
            {
                new EmailDto
                {
                    Subject = (new Random()).Next().ToString(),
                    MessageId = 1,
                    Groups = new List<string> { "1", "2", "3", "4" }
                },
                new EmailDto
                {
                    Subject = (new Random()).Next().ToString(),
                    MessageId = 2,
                    Groups = new List<string> { "1", "2", "3", "4" }
                }
            };


        #endregion

        #region Test methods

        [TestMethod]
        public void Map_WithDtoObject_ReturnsDomainObject()
        {
            // Arrange
            var mapper = new EmailMapper();

            // Act
            var domainObject = mapper.Map(_dtoObject);

            // Assert
            Assert.IsInstanceOfType(domainObject, typeof(Domain.Entities.Email));
        }

        [TestMethod]
        public void Map_WithDtoObjects_ReturnsDomainObjects()
        {
            // Arrange
            var mapper = new EmailMapper();

            // Act
            var domainObjects = mapper.Map(_dtoObjects);

            // Assert
            Assert.IsInstanceOfType(domainObjects, typeof(List<Domain.Entities.Email>));
        }

        [TestMethod]
        public void Map_WithDomainObject_ReturnsDtoObject()
        {
            // Arrange
            var mapper = new EmailMapper();

            // Act
            var dtoObject = mapper.Map(_domainObject);

            // Assert
            Assert.IsInstanceOfType(dtoObject, typeof(EmailDto));
        }

        [TestMethod]
        public void Map_WithDomainObjects_ReturnsDtoObjects()
        {
            // Arrange
            var mapper = new EmailMapper();

            // Act
            var dtoObjects = mapper.Map(_domainObjects);

            // Assert
            Assert.IsInstanceOfType(dtoObjects, typeof(List<EmailDto>));
        }

        #endregion

    }
}
