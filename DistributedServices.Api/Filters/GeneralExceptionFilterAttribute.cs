﻿using DistributedServices.Api.Helpers;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http.Filters;

namespace DistributedServices.Api.Filters
{
    public class GeneralExceptionFilterAttribute : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            if (actionExecutedContext.Exception is Exception)
                actionExecutedContext.Response = new HttpResponseMessage()
                {
                    Content = new StringContent(actionExecutedContext.Exception.Message),
                    StatusCode = HttpStatusCode.InternalServerError
                };

            if (actionExecutedContext.Exception is Exception)
            {
                var response = ApiResponse<object>.InternalServerError("Rysol API Error: Internal Server Error.");

                var content = JsonConvert.SerializeObject(response);

                actionExecutedContext.Response = new HttpResponseMessage
                {
                    StatusCode = HttpStatusCode.InternalServerError,
                    Content = new StringContent(content)
                };
            }
                
        }
    }
}