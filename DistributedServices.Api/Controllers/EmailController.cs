﻿using Application.Services.Emails;
using DistributedServices.Api.Helpers;
using DistributedServices.Entities;
using Domain.Entities;
using Infrastructure.Common.Mappings;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;

namespace DistributedServices.Api.Controllers
{
    [EnableCors("*", "*", "*")]
    [RoutePrefix("api")]
    public class EmailController : ApiController
    {
        private readonly IEmailService _emailService;

        private readonly IMapper<Email, EmailDto> _emailMapper;

        private readonly IMapper<EmailTransaction, EmailTransactionDto> _emailTranMapper;

        public EmailController(IEmailService emailService, IMapper<Domain.Entities.Email, DistributedServices.Entities.EmailDto> emailMapper, IMapper<EmailTransaction, EmailTransactionDto> emailTranMapper)
        {
            _emailService = emailService;

            _emailMapper = emailMapper;

            _emailTranMapper = emailTranMapper;
        }

        [Route("emails")]
        public Response<EmailDto> Post(EmailDto emailDto)
        {
            if (emailDto == null)
                return ApiResponse<EmailDto>.BadRequest("RySol API Error: No email provided.");

            if (emailDto.MessageId == 0)
                return ApiResponse<EmailDto>.BadRequest("RySol API Error: The element message id and value are required.");

            var addedItem = _emailService.Send(_emailMapper.Map(emailDto));

            if (addedItem.Subject == null)
                ApiResponse<EmailDto>.InternalServerError("RySol API Error: Send unsuccessful.");

            return ApiResponse<EmailDto>.Success(_emailMapper.Map(addedItem));
        }

        [Route("emails")]
        public Response<EmailTransactionDto> Post(EmailTransactionDto emailTransactionDto, bool transaction)
        {
            var mappedEmailTransaction = _emailTranMapper.Map(emailTransactionDto);

            var addedEmailTransaction = _emailService.SendTransaction(mappedEmailTransaction);

            var mappedAddedEmailTransaction = _emailTranMapper.Map(addedEmailTransaction);

            return ApiResponse<EmailTransactionDto>.Success(mappedAddedEmailTransaction);
        }
    }
}
