﻿using Domain.Entities;
using System;
using System.Linq;

namespace Application.Services.Emails
{
    public interface IEmailService
    {
        Email Send(Email email);

        EmailTransaction SendTransaction(EmailTransaction emailTransaction);
    }
}
