﻿using Domain.Entities;
using Infrastructure.Data.MainModule.Emails;
using System;
using System.Linq;

namespace Application.Services.Emails
{
    public class EmailService : IEmailService
    {
        private readonly IEmailRepository _emailRepository;

        public EmailService(IEmailRepository emailRepository)
        {
            _emailRepository = emailRepository;
        }

        public Email Send(Email email)
        {
            var item = _emailRepository.Send(email);

            return item;
        }

        public EmailTransaction SendTransaction(EmailTransaction emailTransaction)
        {
            var item = _emailRepository.SendTransaction(emailTransaction);

            return item;
        }
    }
}
