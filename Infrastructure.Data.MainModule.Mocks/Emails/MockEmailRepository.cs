﻿using Domain.Entities;
using Infrastructure.Data.MainModule.Emails;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Infrastructure.Data.MainModule.Mocks.Templates
{
    public class MockEmailRepository : IEmailRepository
    {
        private static List<Domain.Entities.Email> _emails = new List<Domain.Entities.Email>
            {
                new Domain.Entities.Email 
                {
                    Subject = (new Random()).Next().ToString(),
                    MessageId = 1,
                    Groups = new List<string> { "1", "2", "3", "4" }
                },
                new Domain.Entities.Email 
                {
                    Subject = (new Random()).Next().ToString(),
                    MessageId = 2,
                    Groups = new List<string> { "2", "3", "4", "5" }
                }
            };

        public Email Send(Email email)
        {
            return _emails.First();
        }


        public EmailTransaction SendTransaction(EmailTransaction emailTransaction)
        {
            throw new NotImplementedException();
        }
    }
}
