﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EmailApi.Tests.Acceptance.Helpers;
using EmailApi.Tests.Acceptance.Models;
using System.Net.Http;
using System.Collections.Generic;
using System.Net;

namespace EmailApi.Tests.Acceptance
{
    [TestClass]
    public class EmailApiTest
    {
        private const string RootUrl = "http://localhost:49562/api";

        [TestMethod]
        public void PostEmails_WithEmail_ReturnsSuccessAndData()
        {
            // Arrange
            HttpStatusCode httpStatusCode;

            var uriString = string.Format("{0}/{1}", RootUrl, "emails");

            var email = new Email
            {
                Subject = "Test",
                Groups = new List<string>
                {
                    "1", "2", "3"
                },
                MessageId = 1
            };

            // Act
            var response = HttpInvoke.Post<Response<Email>>(new Uri(uriString), email, out httpStatusCode);

            var emailResponse = response.Data;

            var emailStatus = response.HttpStatusCode;

            // Assert
            Assert.IsNotNull(emailResponse);

            Assert.IsTrue(emailStatus == HttpStatusCode.OK);
        }

        [TestMethod]
        public void PostEmailTransaction_WithTransaction_ReturnsSuccessAndData()
        {
            // Arrange
            HttpStatusCode httpStatusCode;

            var uriString = string.Format("{0}/{1}?transaction={2}", RootUrl, "emails", "true");

            var emailTransaction = new EmailTransaction
            {
                EmailAddress = "cnattress@gmail.com",
                Subject = "Test Subject",
                MessageId = 1,
                Groups = new List<string> { "1", "2", "3" },
                CustomFields = new Dictionary<string, string>
                {
                    { "k1", "v1" },
                    { "k2", "v2" },
                    { "k3", "v3" }
                },
                Renter = new Renter
                {
                    People = new List<Person>
                    {
                        new Person
                        {
                            Name = "Brady Cook",
                            Age = "42",
                            Weight = "165",
                            Height = "6ft",
                            ShowSize = "10",
                            HelmetSize = "XL",
                            Package = "SuperSki",
                            OwnBoots = "Yes",
                            DamageWaiver = "Yes",
                            PremiumBoots = "No",
                            Accessories = "No",
                            Total = "$225.00"
                        },
                        new Person
                        {
                            Name = "Cathy",
                            Age = "27",
                            Weight = "105",
                            Height = "5.5ft",
                            ShowSize = "6",
                            HelmetSize = "M",
                            Package = "SuperSki",
                            OwnBoots = "Yes",
                            DamageWaiver = "Yes",
                            PremiumBoots = "No",
                            Accessories = "No",
                            Total = "$225.00"
                        }
                    }
                },
                RentalAccessories = new List<RentalAccessory>
                {
                    new RentalAccessory
                    {
                        Name = "Ski Socks",
                        Amount = "$23.45"
                    },
                    new RentalAccessory
                    {
                        Name = "Burton Gloves",
                        Amount = "$85.01"
                    }
                }
            };

            // Act
            var response = HttpInvoke.Post<Response<EmailTransaction>>(new Uri(uriString), emailTransaction, out httpStatusCode);

            var emailTransactionResponse = response.Data;

            var emailTransactionStatus = response.HttpStatusCode;

            // Assert
            Assert.IsNotNull(emailTransactionResponse);

            Assert.IsTrue(emailTransactionStatus == HttpStatusCode.OK);
        }
    }
}
