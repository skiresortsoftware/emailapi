﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmailApi.Tests.Acceptance.Models
{
    public class Email
    {
        public string Subject { get; set; }
        public List<string> Groups { get; set; }
        public int MessageId { get; set; }
    }

    public class EmailTransaction
    {
        public string EmailAddress { get; set; }
        public string Subject { get; set; }
        public List<string> Groups { get; set; }
        public int MessageId { get; set; }
        public Dictionary<string, string> CustomFields { get; set; }
        public Renter Renter { get; set; }
        public List<RentalAccessory> RentalAccessories { get; set; }
    }

    public class Renter
    {
        public List<Person> People { get; set; }
    }

    public class Person
    {
        public string Name { get; set; }

        public string Age { get; set; }

        public string Weight { get; set; }

        public string Height { get; set; }

        public string ShowSize { get; set; }

        public string HelmetSize { get; set; }

        public string Package { get; set; }

        public string OwnBoots { get; set; }

        public string DamageWaiver { get; set; }

        public string PremiumBoots { get; set; }

        public string Accessories { get; set; }

        public string Total { get; set; }
    }

    public class RentalAccessory
    {
        public string Name { get; set; }

        public string Amount { get; set; }
    }
}
