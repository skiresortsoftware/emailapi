﻿using System;
using System.Linq;

namespace Infrastructure.Common.Configuration
{
    public interface IAuthorizationConfiguration
    {
        bool BasicAuthorizationEnabled { get; }
    }
}
