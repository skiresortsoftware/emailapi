﻿using System;
using System.Configuration;
using System.Linq;

namespace Infrastructure.Common.Configuration
{
    public class AuthorizationConfiguration : IAuthorizationConfiguration
    {
        bool IAuthorizationConfiguration.BasicAuthorizationEnabled
        {
            get { return ConfigurationManager.AppSettings["BasicAuthorizationEnabled"].ToLower() == "true"; }
        }
    }
}
