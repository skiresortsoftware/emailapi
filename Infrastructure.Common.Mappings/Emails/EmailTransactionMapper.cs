﻿using AutoMapper;
using DistributedServices.Entities;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Common.Mappings.Emails
{
    public class EmailTransactionMapper : IMapper<EmailTransaction, EmailTransactionDto>
    {
        public EmailTransaction Map(EmailTransactionDto obj)
        {
            if (obj == null)
                return new EmailTransaction();

            Mapper.CreateMap<EmailTransactionDto, EmailTransaction>();

            return Mapper.Map<EmailTransactionDto, EmailTransaction>(obj);
        }

        public EmailTransactionDto Map(EmailTransaction obj)
        {
            if (obj == null)
                return new EmailTransactionDto();

            Mapper.CreateMap<EmailTransaction, EmailTransactionDto>();

            return Mapper.Map<EmailTransaction, EmailTransactionDto>(obj);
        }


        public List<EmailTransaction> Map(List<EmailTransactionDto> objs)
        {
            if (objs == null)
                return new List<EmailTransaction>();

            var items = objs.Select(obj => Map(obj));

            return items.ToList();
        }

        public List<EmailTransactionDto> Map(List<EmailTransaction> objs)
        {
            if (objs == null)
                return new List<EmailTransactionDto>();

            var items = objs.Select(obj => Map(obj));

            return items.ToList();
        }
    }
}
