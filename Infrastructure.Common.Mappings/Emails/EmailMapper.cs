﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Infrastructure.Common.Mappings.Emails
{
    public class EmailMapper : IMapper<Domain.Entities.Email, DistributedServices.Entities.EmailDto>
    {
        public Domain.Entities.Email Map(DistributedServices.Entities.EmailDto obj)
        {
            if (obj == null)
                return new Domain.Entities.Email();

            return new Domain.Entities.Email
            {
                Subject = obj.Subject,
                MessageId = obj.MessageId,
                Groups = obj.Groups
            };
        }

        public DistributedServices.Entities.EmailDto Map(Domain.Entities.Email obj)
        {
            if (obj == null)
                return new DistributedServices.Entities.EmailDto();

            return new DistributedServices.Entities.EmailDto
            {
                Subject = obj.Subject,
                MessageId = obj.MessageId,
                Groups = obj.Groups
            };
        }


        public List<Domain.Entities.Email> Map(List<DistributedServices.Entities.EmailDto> objs)
        {
            if (objs == null)
                return new List<Domain.Entities.Email>();

            var items = objs.Select(obj => new Domain.Entities.Email
            {
                Subject = obj.Subject,
                MessageId = obj.MessageId,
                Groups = obj.Groups
            });

            return items.ToList();
        }

        public List<DistributedServices.Entities.EmailDto> Map(List<Domain.Entities.Email> objs)
        {
            if (objs == null)
                return new List<DistributedServices.Entities.EmailDto>();

            var items = objs.Select(obj => new DistributedServices.Entities.EmailDto
            {
                Subject = obj.Subject,
                MessageId = obj.MessageId,
                Groups = obj.Groups
            });

            return items.ToList();
        }
    }
}
