﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Domain.Entities
{
    public class Email
    {
        public string Subject { get; set; }
        public List<string> Groups { get; set; }
        public int MessageId { get; set; }
    }
}
